<?php
/**
 * The Header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="main">
 *
 * @package _tk
 */
?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">

	<title><?php wp_title( '|', true, 'right' ); ?></title>

	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">

	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
	<?php do_action( 'before' ); ?>
	<nav class="navbar navbar-default navbar-fixed-top" role="navigation">
		<div class="container">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="<?php echo esc_url(home_url('/'));?>"><?php bloginfo('name'); ?></a>
			</div>
			<div id="navbar" class="collapse navbar-collapse">

<!--				wp menu goes here instead of below -->
				<?php
				wp_nav_menu( array(
					'theme_location' => 'primary',
					'container_class' => 'collapse navbar-collapse navbar-responsive-collapse',
					'menu_class' => 'nav navbar-nav',
					'fallback_cb' => '',
					'menu_id' => 'main-menu',
					'walker' => new wp_bootstrap_navwalker()
				));


//				?>
<!--				<ul class="nav navbar-nav">-->
<!--					<li class="{{set_active('welcome')}}"><a href="{{route('welcome')}}">Home</a></li>-->
<!--					<li class="{{set_active('funding')}}"><a href="{{route('funding')}}">Funding</a></li>-->
<!--					<li class="{{set_active('participation')}}"><a href="{{route('participation')}}">Participation</a></li>-->
<!--					<li class="{{set_active('ministry')}}"><a href="{{route('ministry')}}">Ministry</a></li>-->
<!--					<li class="{{set_active('research')}}"><a href="{{route('research')}}">Research</a></li>-->
<!--					<li class="{{set_active('contact')}}"><a href="{{route('contact')}}">Contact</a></li>-->
<!---->
<!--				</ul>-->
			</div><!--/.nav-collapse -->
		</div>
	</nav>


<div class="main-content">
<?php // substitute the class "container-fluid" below if you want a wider content area ?>
	<div class="container">
		<div class="row">
			<div id="content" class="main-content-inner col-sm-12 col-md-8">

