<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, and ABSPATH. You can find more information by visiting
 * {@link http://codex.wordpress.org/Editing_wp-config.php Editing wp-config.php}
 * Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'wp_db');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'shs123');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         ',;ky~VZz|n7wW*J~hL,|3E`,[No/^|6Yy_O~q%8TLi|9n:JI<`iPavv?+/.LaP47');
define('SECURE_AUTH_KEY',  '#_,Z,R;}b}cn(vz;Z 6T6q.#SUIY9Q2X=@6;2&^a&WEm7., q.3*v$(ToQWghFp:');
define('LOGGED_IN_KEY',    '+!NQ~_jC{5VkM9_c{[]m=cze>QoSrZ4ok6Er3LmL7NI:4/~8$djpv!9&5npN%>8y');
define('NONCE_KEY',        '{+*1EC[i>W1R.d;a;A-g> }q~kJ9pBwi`fI$YOb#{J b<XM(/;+i#iR[U4sO>F?q');
define('AUTH_SALT',        '?`]<c3v,Ys _H6yP-a.N4p2/FF]G6Yrdful1So4M+JQ%BQ0~0i6f,r!NE|fwDSq,');
define('SECURE_AUTH_SALT', '=go>.`[&^$4S4RM6V<5c(%1/HD_u~S%(er12y+5]N@1V$].$]e)ZoG?5wSq43~cE');
define('LOGGED_IN_SALT',   'tq8?1*$z^5p0:u&*,6pwHna$uo.XFP`2U?9Ibj -9uhs4Q+O=5$rhE,VXI+2.P3I');
define('NONCE_SALT',       'qcSS<P)pX1iDbKd(f|#[;b}U*!nMCx:gwQD.#EP%<]3kN*w8y1%[{s;fhxr4mVpC');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', false);
define('WP_ALLOW_MULTISITE', true);
define('MULTISITE', true);
define('SUBDOMAIN_INSTALL', false);
define('DOMAIN_CURRENT_SITE', 'dev.app');
define('PATH_CURRENT_SITE', '/wp/html/');
define('SITE_ID_CURRENT_SITE', 1);
define('BLOG_ID_CURRENT_SITE', 1);
/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
